================
 bepasty-server
================

----------------------------------------------------
Run a bepasty server through the builtin WSGI server
----------------------------------------------------

:Manual section: 1

SYNOPSIS
========

   bepasty-server [-h] [--host HOST] [--port PORT] [--debug]

DESCRIPTION
===========

bepasty-server runs bepasty through the builtin WSFI server from flask.

Note that this is not suitable for production, where a WSGI server such
as gunicorn, apache+mod-wsgi, nginx+uwsgi, etc. is recommended.

OPTIONS
=======

-h, --help   show this help message and exit
--host HOST  Host to listen on
--port PORT  Port to listen on
--debug      Activate debug mode

ENVIRONMENT
===========

BEPASTY_CONFIG
   Absolute path to a bepasty configuration file

SEE ALSO
========

* `Bepasty documentation <https://bepasty-server.readthedocs.io/en/latest/>`_
