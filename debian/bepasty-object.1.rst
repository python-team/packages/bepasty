================
 bepasty-object
================

--------------------------------
Manage objects stored by bepasty
--------------------------------

:Manual section: 1

SYNOPSIS
========

   bepasty-object [-h] [--config CONFIG] {migrate,purge,consistency,info,set} ... NAME [NAME ...]

DESCRIPTION
===========

bepasty-object operates on objects stored in the bepasty storage.

All commands expect either a ``--config <configfilename>`` argument or
that the ``BEPASTY_CONFIG`` environment variable points to your
configuration file.

OPTIONS
=======

-h, --help            show this help message and exit
--config CONFIG       bepasty configuration file

COMMANDS
========

migrate
-------

migrate object to the current metadata schema, in case of a bebasty
upgrade.

Run as::

   bepasty-object migrate '*'

Note: the ‘*’ needs to be quoted with single-quotes so the shell does
not expand it. it tells the command to operate on all names in the
storage (you could also give some specific names instead of ‘*’).

purge
-----

Purge objects from the database; multiple conditions are ANDed together.

-D, --dry-run
   do not remove anything, just display what would happen
-A PURGE_AGE, --age PURGE_AGE
   only remove if upload older than PURGE_AGE days
-I PURGE_INACTIVITY, --inactivity PURGE_INACTIVITY
   only remove if latest download older than PURGE_INACTIVITY days
-S PURGE_SIZE, --size PURGE_SIZE
   only remove if file size > PURGE_SIZE MiB
-T PURGE_TYPE, --type PURGE_TYPE
   only remove if file mimetype starts with PURGE_TYPE

consistency
-----------

Run consistency checks.

-C, --compute
   compute missing hashes and write into metadata
-F, --fix
   write computed hash/size into metadata
-R, --remove
   remove files with inconsistent hash/size

info
----

Display information about objects.

set
---

Set flags on objects.

-L, --lock
-l, --unlock
-C, --incomplete
-c, --complete

ENVIRONMENT
===========

BEPASTY_CONFIG
   Absolute path to a bepasty configuration file

SEE ALSO
========

* `Bepasty documentation <https://bepasty-server.readthedocs.io/en/latest/>`_
